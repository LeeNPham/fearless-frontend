import Nav from './Nav'
import React from "react";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendForm from './AttendForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route, Outlet, NavLink, Link } from "react-router-dom";
import { render } from "react-dom";



// import logo from './logo.svg';
// import './App.css';

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//       <Nav />
//       <AttendForm />
//       <LocationForm />
//       <ConferenceForm />
//       <div className="container">
//         <AttendeesList attendees={props.attendees} />
//       </div>
//     </>
//   );
// }


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
      {/* </div> */}
    </BrowserRouter>
  );
}

export default App;

















// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> THIS CODE, MY CODE, IT LOADSSSS
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

// move bottom app function component over to attendeeslist.js
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <React.Fragment>
//     <Nav />

//     <div className="container ">
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           {/*
//           this is for the for loop but we want the internal function thing
//           for (let attendee of props.attendees) {
//             <tr>
//               <td>{ attendee.name }</td>
//               <td>{ attendee.conference }</td>
//             </tr>
//           } 
//           */}
//           {/* Whenever you use the map method, you have to add a key so
//           that React can keep track of the things it's generating. 
//           If you add a key attribute to the tr tag and give it a value unique to the attendee, 
//           the error should go away. */}
//           {props.attendees.map(attendee => {
//             return (
//               <tr key={attendee.href}>
//                 <td>{attendee.name}</td>
//                 <td>{attendee.conference}</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     </div>
//     </React.Fragment>
//   );
// }

// export default App;