# ConferenceGO!
Designed and Created By
*Lee Pham*

## Intended Market

ConferenceGO! has been created for companies that host conferences throughout the nation to easily manage and maintain conferences as multiple locations.

## Functionality

New Conferences can be created and assigned for different locations. Presentations for these conferences can also be created. To which attendees may sign up to view these presentations.

## Installation

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create postgres-data
4. Run docker compose build
5. Run docker compose up
